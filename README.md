# commentsOnSurvey : allow adding comments on survey by admin user. #

## Installation

This plugin was tested on LimeSurvey 3.17.3 version and need 3.X version.

### Via GIT
- Go to your LimeSurvey Directory
- Clone in plugins/commentsOnSurvey directory : `git clone https://gitlab.com/SondagesPro/management/commentsOnSurvey.git commentsOnSurvey`

### Via ZIP dowload
- Get the file [commentsOnSurvey.zip](https://dl.sondages.pro/commentsOnSurvey.zip)
- Extract : `unzip commentsOnSurvey.zip`
- Move the directory to plugins/ directory inside LimeSurvey

### Activate

You just need to activate plugin like other plugin, see [Install and activate a plugin for LimeSurvey](https://extensions.sondages.pro/install-and-activate-a-plugin-for-limesurvey.html).

## Usage

With token enable survey with answers persistance and allow update response : user can come back to survey again and again.

LimeSurvey admin user can update too answers of participant : this plugin allow LimeSurvey admin user to add comments on each questions.

You can allow token user to add or update comments, the minimal right for token user is allow delete (set asd read) the existing comment.

## Contribute

Issue and pull request are welcome on [gitlab](https://gitlab.com/SondagesPro/management/commentsOnSurvey).

## Home page & Copyright

- HomePage <http://extensions.sondages.pro/>
- Copyright © 2019 Denis Chenu <https://sondages.pro>
- Copyright © 2019 OECD <https://oecd.org>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
