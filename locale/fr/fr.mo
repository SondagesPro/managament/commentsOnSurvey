��          t      �         B        T     ]  ?   x     �     �  .   �       '        F  �  _  g   $     �  '   �  N   �          )  >   ;     z  .   �     �     	                               
                        Allow limesurvey admin user to create or update comment in survey. Comments Comments on this question. If you disable this option : this disable totally the comments. Leave default (%s) Set as read. Show the comment according to survey settings. Show the comment area. This will definitely remove the comment Where comment is append. Project-Id-Version: commentsOnSurvey
PO-Revision-Date: 2024-02-08 12:31+0100
Last-Translator: Denis Chenu
Language-Team: Sondages Pro
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.2.2
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: translate
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: views
 Permettre aux administrateurs LimeSurvey de créer ou mettre à jour les commentaires du questionnaire. Commentaires Commentaires concernant cette question. Si vous désactivez cette option: cela désactive totalement les commentaires. Laisser par défaut (%s) Marquer comme lu. Montrer le commentaire selon les paramètres du questionnaire. Montrer le champs commentaire. Ceci supprimera définitivement le commentaire Ou le commentaire est ajouté. 