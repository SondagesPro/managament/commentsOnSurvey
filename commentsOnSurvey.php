<?php
/**
 * Allow to put comments on survey for each response
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2019-2023 Denis Chenu <https://www.sondages.pro>
 * @copyright 2019 OECD <https://www.oecd.org/>
 * @license AGPL v3
 * @version 1.2.2
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class commentsOnSurvey extends PluginBase
{
    protected static $description = 'Allow admin user or token user to put comments on survey';
    protected static $name = 'commentsOnSurvey';

    protected static $dbVersion = 1;

    protected $storage = 'DbStorage';

    protected $settings = array(
        'information' => array(
            'type' => 'info',
            'content' => 'The default settings for all surveys. You can update it in each survey plugin setting.',
        ),
        'allowAdminUser' => array(
            'type'=>'checkbox',
            'htmlOptions'=>array(
                'value'=>1,
                'uncheckValue'=>0,
            ),
            'label' => "Allow limesurvey admin user to create or update comment in survey.",
            'help' => "If you disable this option : this disable totally the comments.",
            'default'=>1,
        ),
        //~ 'allowGroupManagerUser' => array(
            //~ 'type'=>'checkbox',
            //~ 'htmlOptions'=>array(
                //~ 'value'=>1,
                //~ 'uncheckValue'=>0,
            //~ ),
            //~ 'label'=>"Allow group manager to comment on survey.",
            //~ 'help' => "Only available with responseListAndManage plugin",
            //~ 'default'=>0,
        //~ ),
        'allowTokenUserCreate' => array(
            'type'=>'checkbox',
            'htmlOptions'=>array(
                'value'=>1,
                'uncheckValue'=>0,
            ),
            'label'=>"Allow participant to create comment in survey.",
            'default'=>0,
        ),
        'allowTokenUserUpdate' => array(
            'type'=>'checkbox',
            'htmlOptions'=>array(
                'value'=>1,
                'uncheckValue'=>0,
            ),
            'label'=>"Allow participant to update existing comments in survey.",
            'help' => "Always allow participant to set comments as read.",
            'default'=>0,
        ),
        //~ 'keepAllComments' => array(
            //~ 'type'=>'checkbox',
            //~ 'htmlOptions'=>array(
                //~ 'value'=>1,
                //~ 'uncheckValue'=>0,
            //~ ),
            //~ 'label'=>"Keep in database all comments done.",
            //~ 'default'=>0,
        //~ ),
    );
    public function init()
    {
        /* DB creation */
        $this->subscribe('beforeActivate');

        /* Don't do other action on console */
        if (Yii::app() instanceof CConsoleApplication) {
            return;
        }

        /* Register the model */
        $this->subscribe('afterPluginLoad');

        /* question part */
        $this->subscribe('beforeQuestionRender', 'addCommentsInQuestionRender');
        $this->subscribe('newQuestionAttributes', 'addCommentAttribute');

        /* Survey settings */
        $this->subscribe('beforeSurveySettings');
        $this->subscribe('newSurveySettings');
        
        //$this->subscribe('newDirectRequest');
        /* Manage submit */
        $this->subscribe('beforeSurveyPage');
    }

    /** @inheritdoc **/
    public function getPluginSettings($getValues = true)
    {
        if(!Permission::model()->hasGlobalPermission('settings','read')) {
            throw new CHttpException(403);
        }
        /* @todo translation of label and help */
        return parent::getPluginSettings($getValues);
    }

    /** @inheritdoc **/
    public function saveSettings($settings)
    {
        if(!Permission::model()->hasGlobalPermission('settings','update')) {
            throw new CHttpException(403);
        }
        return parent::saveSettings($settings);
    }

    /**
     * Create the DB when activate
     */
    public function beforeActivate()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $this->getEvent()->set("success", $this->_setDb());
    }

    public function afterPluginLoad()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        //~ $twigRenderer = Yii::app()->getComponent('twigRenderer');
        //~ $twigRenderer->sandboxConfig['methods']['ETwigViewRendererStaticClassProxy'][] = 'getIdByName';
        //~ Yii::app()->setComponent('twigRenderer',$twigRenderer);
    }

    /* @see plugin event */
    public function getPluginTwigPath()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $viewPath = dirname(__FILE__)."/views";
        $this->getEvent()->append('add', array($viewPath));
        $this->unsubscribe('getPluginTwigPath');
    }

    /**
     * Show the survey settings
     */
    public function beforeSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oEvent = $this->event;
        $surveyId = $oEvent->get('survey');
        $default = array(
            'allowAdminUser' => $this->get('allowAdminUser', null, null, $this->settings['allowAdminUser']['default']) ? gT("Yes") : gT("No"),
            'allowTokenUserCreate' => intval($this->get('allowTokenUserCreate', null, null, $this->settings['allowTokenUserCreate']['default'])) ? gT("Yes") : gT("No"),
            'allowTokenUserUpdate' => intval($this->get('allowTokenUserUpdate', null, null, $this->settings['allowTokenUserUpdate']['default'])) ? gT("Yes") : gT("No"),
        );
        $oEvent->set("surveysettings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => array(
                'allowAdminUser' => array(
                    'type'=>'select',
                    'options' => array(
                        1=>gT("Yes"),
                        0=>gT("No"),
                    ),
                    'htmlOptions'=>array(
                        'empty'=> sprintf($this->translate("Leave default (%s)"), $default['allowAdminUser']),
                    ),
                    'label' => $this->translate("Allow limesurvey admin user to create or update comment in survey."),
                    'help' => $this->translate("If you disable this option : this disable totally the comments."),
                    'current'=>$this->get('allowAdminUser', 'Survey', $surveyId, ''),
                ),
                //~ 'allowGroupManagerUser' => array(
                    //~ 'type'=>'checkbox',
                    //~ 'htmlOptions'=>array(
                        //~ 'value'=>1,
                        //~ 'uncheckValue'=>0,
                    //~ ),
                    //~ 'label'=>"Allow group manager to comment on survey.",
                    //~ 'help' => "Only available with responseListAndManage plugin",
                    //~ 'default'=>0,
                //~ ),
                'allowTokenUserCreate' => array(
                    'type'=>'select',
                    'options' => array(
                        1=>gT("Yes"),
                        0=>gT("No"),
                    ),
                    'htmlOptions'=>array(
                        'empty'=> sprintf($this->translate("Leave default (%s)"), $default['allowTokenUserCreate']),
                    ),
                    'label'=>"Allow participant to create comment in survey.",
                    'current'=>$this->get('allowTokenUserCreate', 'Survey', $surveyId, ''),
                ),
                'allowTokenUserUpdate' => array(
                    'type'=>'select',
                    'options' => array(
                        1=>gT("Yes"),
                        0=>gT("No"),
                    ),
                    'htmlOptions'=>array(
                        'empty'=> sprintf($this->translate("Leave default (%s)"), $default['allowTokenUserUpdate']),
                    ),
                    'label'=>"Allow participant to update existing comments in survey.",
                    'help' => "Always allow participant to set comments as read.",
                    'current'=>$this->get('allowTokenUserCreate', 'Survey', $surveyId, ''),
                ),
            ),
        ));
    }

    /**
     * Save the survey settings
     */
    public function newSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->event;
        foreach ($event->get('settings') as $name => $value) {
            $this->set($name, $value, 'Survey', $event->get('survey'));
        }
    }

    /**
     * Add the textarea and the checkbox inside question HTML
     */
    public function addCommentsInQuestionRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $renderEvent = $this->getEvent();
        $sid = $this->getEvent()->get('surveyId');
        if (empty($_SESSION['survey_'.$sid]['srid'])) {
            return;
        }
        if (!$this->_getSurveySetting('allowAdminUser', $sid)) {
            return;
        }

        $srid = $_SESSION['survey_'.$sid]['srid'];
        $qid = $renderEvent->get('qid');
        $aAttributes = \QuestionAttribute::model()->getQuestionAttributes($qid);
        if (!empty($aAttributes['commentsOnSurvey_comments'])) {
            $isAdmin = \Permission::model()->hasSurveyPermission($sid, 'responses', 'update');
            $currentComment = \commentsOnSurvey\models\questionsComments::getCurrentComment($sid, $qid, $srid);
            if (empty($currentComment) && !$isAdmin && !$this->_getSurveySetting('allowTokenUserCreate', $sid)) {
                return;
            }

            $allowCreate = ($isAdmin || ($this->_getSurveySetting('allowTokenUserCreate', $sid) && empty($currentComment->comment)));
            $allowUpdate = ($isAdmin || ($this->_getSurveySetting('allowTokenUserUpdate', $sid) && !empty($currentComment->comment)));
            $showDelete = (!$allowUpdate && !empty($currentComment->comment));
            $aSurveyinfo = getSurveyInfo($sid, App()->getLanguage());
            /* event are updated to getPluginTwigPath */
            $this->subscribe('getPluginTwigPath');
            $commentsHtml = Yii::app()->twigRenderer->renderPartial('/subviews/survey/question_subviews/comments_on_survey.twig', array(
                'aSurveyInfo' => getSurveyInfo($sid, App()->getLanguage()),
                'allowCreate' => $allowCreate,
                'allowUpdate' => $allowUpdate,
                'showDelete' => $showDelete,
                'currentComment' => $currentComment,
                'commentName' => 'commentOnSurvey['.$qid.']',
                'setName' => 'commentOnSurveySet['.$qid.']',
                'commentId' => \CHtml::getIdByName('commentOnSurvey['.$qid.']'),
                'deleteName' => 'commentOnSurveyDelete['.$qid.']',
                'deleteId' => \CHtml::getIdByName('commentOnSurveyDelete['.$qid.']'),
                'lang'=> array(
                    'Comments on this question.' => $this->translate('Comments on this question.'),
                    'Set as read.' => $this->translate('Set as read.'),
                    'This will definitely remove the comment' => $this->translate('This will definitely remove the comment'),
                ),
            ));
            $blockAppend = empty($aAttributes['commentsOnSurvey_append']) ? 'text' : $aAttributes['commentsOnSurvey_append'];
            $renderEvent->set($blockAppend, $renderEvent->get($blockAppend)."\n\n".$commentsHtml);
        }
    }

    /**
     * Add the option in each question
     */
    public function addCommentAttribute()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $questionAttributes['commentsOnSurvey_comments']=array(
            "types"=>'15ABCDEFGHIKLMNOPQRSTUWYZ!:;|X*',
            'category'=>$this->translate('Comments'),
            'sortorder'=>10,
            'inputtype'=>'switch',
            'options'=>array(0=>gT('No'), 1=>gT('Yes')),
            'default'=>0,
            "help"=>$this->translate("Show the comment according to survey settings."),
            "caption"=>$this->translate('Show the comment area.'),
        );
        $questionAttributes['commentsOnSurvey_append']=array(
            "types"=>'15ABCDEFGHIKLMNOPQRSTUWYZ!:;|X*',
            'category'=>$this->translate('Comments'),
            'sortorder'=>20,
            'inputtype'=>'singleselect',
            'options'=>array(
                'text'=>gT('Question'),
                'help'=>gT('Help'),
                'answers'=>gT('Answers'),
            ),
            'default'=>'text',
            "help"=>"",//$this->translate("Show the comment according to survey settings."),
            "caption"=>$this->translate('Where comment is append.'),
        );
        $this->getEvent()->append('questionAttributes', $questionAttributes);
    }

    /**
     * Manage POST values
     */
    public function beforeSurveyPage()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $commentsOnSurvey = App()->getrequest()->getPost('commentOnSurvey', array());
        $commentsOnSurveySet = App()->getrequest()->getPost('commentOnSurveySet', array());
        $commentsOnSurveyDelete = App()->getrequest()->getPost('commentOnSurveyDelete', array());
        if (empty($commentsOnSurveySet) && empty($commentsOnSurveyDelete)) {
            return;
        }
        $surveyId = $this->getEvent()->get('surveyId');

        if (!$this->_getSurveySetting('allowAdminUser', $surveyId)) {
            return;
        }
        if (empty($_SESSION['survey_'.$surveyId]['srid'])) {
            return;
        }
        $srid = $_SESSION['survey_'.$surveyId]['srid'];

        $createRight = \Permission::model()->hasSurveyPermission($surveyId, 'responses', 'update') || $this->_getSurveySetting('allowTokenUserCreate', $surveyId);
        $updateRight = \Permission::model()->hasSurveyPermission($surveyId, 'responses', 'update') || $this->_getSurveySetting('allowTokenUserUpdate', $surveyId);
        foreach ($commentsOnSurveySet as $qid => $commentOnSurveySet) {
            $commentOnSurvey = isset($commentsOnSurvey[$qid]) ? $commentsOnSurvey[$qid] : "";
            // Todo : check if allowed
            $currentComment = \commentsOnSurvey\models\questionsComments::getCurrentComment($surveyId, $qid, $srid);
            // Todo : add a "update" checkbox : more clear and simple and less control is always better
            if (!empty($currentComment)) {
                if ($this->_replaceNewLine($commentOnSurvey) == $this->_replaceNewLine($currentComment->comment)) {
                    continue;
                }
                if (!$updateRight) {
                    continue;
                }
            }
            if (empty($currentComment)) {
                if (!$createRight) {
                    continue;
                }
            }
            $oComment = \commentsOnSurvey\models\questionsComments::setCurrentComment($surveyId, $qid, $srid, $commentOnSurvey);
        }
        foreach ($commentsOnSurveyDelete as $qid => $commentOnSurveyDelete) {
            \commentsOnSurvey\models\questionsComments::setCommentAsRead($surveyId, $qid, $srid);
        }
    }

    /**
     * Create or update the DB
     */
    private function _setDb()
    {
        if ($this->get("dbVersion") >= self::$dbVersion) {
            return;
        }
        $sCollation = '';
        if (Yii::app()->db->driverName == 'mysql' || Yii::app()->db->driverName == 'mysqli') {
            $sCollation = "COLLATE 'utf8mb4_bin'";
        }
        if (Yii::app()->db->driverName == 'sqlsrv'
            || Yii::app()->db->driverName == 'dblib'
            || Yii::app()->db->driverName == 'mssql') {
            $sCollation = "COLLATE SQL_Latin1_General_CP1_CS_AS";
        }
        /* dbVersion not needed */
        if (!$this->api->tableExists($this, 'questionsComments')) {
            $this->api->createTable($this, 'questionsComments', array(
                'id'=>'pk',
                'sid'=>'int not NULL',
                'qid'=>'int not NULL',
                'srid'=>'int not NULL',
                'comment'=>'text',
                'created'=>'datetime', // DEFAULT CURRENT
                'readed'=>'datetime', // DEFAULT NULL ?
                'authortype' => 'string(1)',
                'author' => "string(35) {$sCollation}",
            ));
            //$tableName
            Yii::app()->getDb()->createCommand()->createIndex('{{idx1_questionscomments}}', '{{commentsonsurvey_questionsComments}}', ['qid', 'sid','srid'], false);
            Yii::app()->getDb()->createCommand()->createIndex('{{idx2_questionscomments}}', '{{commentsonsurvey_questionsComments}}', ['qid', 'sid','srid','created'], false);
            Yii::app()->getDb()->createCommand()->createIndex('{{idx3_questionscomments}}', '{{commentsonsurvey_questionsComments}}', ['qid', 'sid','srid','readed'], false);
            $this->set("dbVersion", 1);
        }
        $this->set("dbVersion", self::$dbVersion);
    }

    /**
     * Get final setting for survey
     * @param string setting to get
     * @param integer survey id
     * @return mixed
     */
    private function _getSurveySetting($setting, $surveyid)
    {
        static $values =[];
        if (!isset($values[$surveyid])) {
            $values[$surveyid] = [];
        }
        if (isset($values[$surveyid][$setting])) {
            return $values[$surveyid][$setting];
        }
        $values[$surveyid][$setting] = $this->get(
            $setting,
            'Survey',
            $surveyid,
            ''
        );
        if ($values[$surveyid][$setting] === "") {
            $values[$surveyid][$setting] = $this->get($setting, null, null, $this->settings[$setting]['default']);
        }
        return $values[$surveyid][$setting];
    }
    /**
     * Translate a string
     * @param string to translate
     * @param string escape mode
     * @parm string language to use
     * @return string
     */
    private function translate($string, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        if (intval(Yii::app()->getConfig('versionnumber')) >= 3) {
            return parent::gT($string, $sEscapeMode, $sLanguage);
        }
        return $string;
    }

    /**
     * replace new line to specific new line, used to compare strings difference between loaded by DB and send by browser
     * @param string $string to fix
     * @param string $newline replace by
     * @return string
     */
    private static function _replaceNewLine($string, $newline = "\n")
    {
        if (version_compare(substr(PCRE_VERSION, 0, strpos(PCRE_VERSION, ' ')), '7.0')>-1) {
            return trim(preg_replace(array('~\R~u'), array($newline), $string));
        }
        return trim(str_replace(array("\r\n","\n", "\r"), array($newline,$newline,$newline), $string));
    }
}
